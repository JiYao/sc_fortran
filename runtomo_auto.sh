#!/bin/bash

# SRUN OPTIONS
# NOTE: #SBATCH is a command, not a comment
# ----------------------------------------------------------------------
# NECESSARY PARAMETERS

# The number of tasks
#SBATCH -n 1

# ----------------------------------------------------------------------
# OPTIONAL PARAMETERS

# The number of cores per task (used for multi-threaded application, default=1, 
# prefer a power of 6, no more than the max number of cores per node)
# Change this when using openMP in camb for CosmoMC.  Only advantageous when calculating 
# CMB and MPK
#SBATCH -c 6
##SBATCH -c 8
# The number of tasks per node (n * c)
# Can uncomment this to force x number of tasks per node.
##SBATCH --tasks-per-node=1

# The number of nodes
# SLURM issues a warning if you specify more nodes than tasks (N > n). 
# It's better to let slurm calculate it for you.
# IMPORTANT: MPI between nodes will slow down the program because of the 
# heavy I/O. It's recommended to more cores and less nodes for our jobs. 
# Can change below to run MPI jobs across multiple nodes
#SBATCH -N 1
##SBATCH -N 4

# Use --exclusive to get the whole nodes exclusively for this job
##SBATCH --exclusive

# Request a sepcific list of hosts (two different formats)
##SBATCH -w f7
##SBATCH -w ./hostfile
## Request a specific list of hosts NOT be included
#SBATCH --exclude=cosmo,f1
# Setting the name of the error-file to 'job.myjobid.err' and 
# the output-file to 'job.myjobid.out'
# The file paths below are relative to the directory from which you submitted
# Change to your preferences.
#SBATCH --error=%J.err --output=%J.out

#Print  detailed  event  logging to error file
#SBATCH -v

#Give your job a name, so you can more easily identify which job is which
#SBATCH -J AutoSpec
# ------------------------------
# VERY OPTIONAL PARAMETERS

# Account name (project ID) to run under
##SBATCH -A <account>

# The maximum allowed run time (D-HH:MM:)
##SBATCH --time=15-00:00:00

# If this job needs 4GB of memory per mpi-task (=mpi ranks, =cores)
##SBATCH --mem-per-cpu=4000

# ----------------------------------------------------------------------
# MPI SET-UP FOR SLURM

# Different types of MPI may result in unique initiation procedures.
# IMPORTANT for mpich2: user assumes the system administrator already link
# your program with SLURM's implementation of the PMI library
# If SLURM is not configured with MpiDefault=pmi2
# then the srun command MUST BE invoked with the option --mpi=pmi2.
# Reference: http://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_How_do_I_use_MPICH_with_slurm.3F
# Reference: http://slurm.schedmd.com/mpi_guide.html#mpich2
# ---------------------------------------------------------------------



# Run the MPI application

#uncomment the line below if you want to run with openMP as advised above.  
#change number of threads to match -c line above.
#export OMP_NUM_THREADS=6

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Current working directory is `pwd`"

# Changing to director where my application is since I submit from scripts

##rm generateinputfiles_spec.out
##ifort generateinputfiles_spec.f -o generateinputfiles_spec.out

##rm spec_tomo.out
##ifort spec_tomo.f -o spec_tomo.out

##rm tomo_input.out
##ifort tomo_input.f -o tomo_input.out
##./tomo_input.out < generateinputfiles_spec.in

# ------------------------------------------------------------

# ------------------------------------------------------------

mkdir ./Spectrums/101/
cd Spectrums/101/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_101.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/202/
cd Spectrums/202/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_202.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/303/
cd Spectrums/303/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_303.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/404/
cd Spectrums/404/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_404.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/505/
cd Spectrums/505/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_505.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/606/
cd Spectrums/606/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_606.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/707/
cd Spectrums/707/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_707.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/808/
cd Spectrums/808/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_808.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/909/
cd Spectrums/909/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_909.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..

# ------------------------------------------------------------

mkdir ./Spectrums/1010/
cd Spectrums/1010/
rm generateinputfiles_spec.out
cp ../../generateinputfiles_spec.out generateinputfiles_spec.out
rm spec_tomo.out
cp ../../spec_tomo.out spec_tomo.out
./generateinputfiles_spec.out < ../../bins/tomo_1010.in

./spec_tomo.out < C_GG.in > C_GG.dat #1 (GGii)
./spec_tomo.out < C_GGN.in > C_GGN.dat #7 (GGNii)

# GG spec
./spec_tomo.out < GG_1p.in > GG_1p.dat
./spec_tomo.out < GG_1m.in > GG_1m.dat

./spec_tomo.out < GG_2p.in > GG_2p.dat
./spec_tomo.out < GG_2m.in > GG_2m.dat

./spec_tomo.out < GG_3p.in > GG_3p.dat
./spec_tomo.out < GG_3m.in > GG_3m.dat

./spec_tomo.out < GG_4p.in > GG_4p.dat
./spec_tomo.out < GG_4m.in > GG_4m.dat

./spec_tomo.out < GG_7p.in > GG_7p.dat
./spec_tomo.out < GG_7m.in > GG_7m.dat

./spec_tomo.out < GG_8p.in > GG_8p.dat
./spec_tomo.out < GG_8m.in > GG_8m.dat

cd ../..



#matlab -nodesktop -nosplash -r "plot_spec_result; FisherMatrix_C1;"

echo "Program finished with exit code $? at: `date`"

# ---------------------------------------------------
# Reference: http://www.hpc2n.umu.se/batchsystem/examples_scripts,
#            http://www.hpc2n.umu.se/slurm-submit-file-design
#            https://computing.llnl.gov/tutorials/linux_clusters/man/srun.txt
# ---------------------------------------------------
