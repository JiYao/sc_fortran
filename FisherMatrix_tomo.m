clc;clear;close all;

% take C^(1)=GG spectrum, and C^GG=C^(1)-C^IG. IG is anti-correlation, and
% recall we normally ignore the - sign in spectrum calculation. Thus the
% actual C^GG we want is calculated by GG+IG spectrum

%% load fiducial spectrums

p=[0.315 0.211995 0.829 0.9603 0 0.049 -1.0 0 0 0.05 1.0];
% parameters: Omega_m Omega_m*h sigma8 xn alpha_s z0 w_0 w_a Delta_z sigma_z A0

partial=0.15;
dalphas=0.1;
dwa=0.3;
domegab=0.01;
dDz=0.01;
bi=1;

fsky=0.436;

filename=fullfile('Spectrums1',int2str(101),'C_GG.dat');
load(filename);
l=C_GG(:,1);
slt=find(20<=l & l<=5000);
l=l(slt);

cb=[3:9]; % choose bins

F=zeros(6,6);
for alpha=1:6 % 6 parameters
	for beta=1:6
	
		C=zeros(10,10,length(l)); % 10 photo-z bins
		dPdalpha=zeros(10,10,length(l));
		dPdbeta=dPdalpha;
		for i=cb
			for j=i:max(cb)
			
				filename=fullfile('Spectrums1',int2str(i*100+j),'C_GG.dat');
				C_GG=load(filename);
				C(i,j,:)=C_GG(slt,2);
				if i==j
					filename=fullfile('Spectrums1',int2str(i*100+j),'C_GGN.dat');
					load(filename);
					C(i,j,:)=C_GG(slt,2)+C_GGN(slt,2);
				end;
				C(j,i,:)=C(i,j,:);
				
				alpha1=alpha;
				beta1=beta;
				if alpha>4 alpha1=alpha+2; end;
				if beta>4 beta1=beta+2; end;
				
				t=['GG_',int2str(alpha1),'p.dat'];
				filename=fullfile('Spectrums1',int2str(i*100+j),t);
				parap=load(filename);
				
				t=['GG_',int2str(alpha1),'m.dat'];
				filename=fullfile('Spectrums1',int2str(i*100+j),t);
				param=load(filename);
				
				dPdalpha(i,j,:)=(parap(slt,2)-param(slt,2))./(2*partial*p(1));
				dPdalpha(j,i,:)=dPdalpha(i,j,:);
				
				t=['GG_',int2str(beta1),'p.dat'];
				filename=fullfile('Spectrums1',int2str(i*100+j),t);
				parap=load(filename);
				
				t=['GG_',int2str(beta1),'m.dat'];
				filename=fullfile('Spectrums1',int2str(i*100+j),t);
				param=load(filename);
				
				dPdbeta(i,j,:)=(parap(slt,2)-param(slt,2))./(2*partial*p(1));
				dPdbeta(j,i,:)=dPdbeta(i,j,:);
		
			end;
		end;
		
		C=C(cb,cb,:);
		dPdalpha=dPdalpha(cb,cb,:);
		dPdbeta=dPdbeta(cb,cb,:);
		
		for i=1:length(l)
			F(alpha,beta)=F(alpha,beta) + (l(i)+1/2)*0.2*l(i)*fsky*trace( inv(C(:,:,i))*dPdalpha(:,:,i)*inv(C(:,:,i))*dPdbeta(:,:,i) );
		end;
	end;
end;

%F=F([1,3:6],[1,3:6]);
C2=inv(F);
%C2=C2([1,3:6],[1,3:6]);

IP=[1:4,7:8];
p=p(IP);
indx=[1 3 2 5 6];%[1 3 2 5 6]; % plot index
names={'\Omega_m','\Omega_m*h','\sigma_8','n_s','w_0','w_a'};
num_para=length(indx);
range=4; % +/- 4 sigma
figure;
ps=500; % plot-sizefigure;
set(gcf,'Position',[0,0,ps*num_para,ps*num_para]);

p(2)=p(2)/p(1); % \Omega_m *h --> h
M=eye(length(IP));
M(1,2)=0; M(2,1)=p(2); M(2,2)=p(1);
F=M'*F*M;
names{2}='h_0';

for i=1:num_para-1
	for j=i+1:num_para

		subplot('Position',[1/num_para*i,1/num_para*(num_para+1-j),1/num_para,1/num_para]);
		p1=linspace( p(indx(i))-range*sqrt(C2(indx(i),indx(i))), p(indx(i))+range*sqrt(C2(indx(i),indx(i))), 500 );
		p2=linspace( p(indx(j))-range*sqrt(C2(indx(j),indx(j))), p(indx(j))+range*sqrt(C2(indx(j),indx(j))), 500 );
		[X,Y]=meshgrid(p1,p2);
		
		rho=C2(indx(i),indx(j))/sqrt(C2(indx(i),indx(i)))/sqrt(C2(indx(j),indx(j)));
		chi2=( (X-p(indx(i))).^2./C2(indx(i),indx(i)) + (Y-p(indx(j))).^2./C2(indx(j),indx(j)) -2*rho*(X-p(indx(i)))/sqrt(C2(indx(i),indx(i))).*(Y-p(indx(j)))/sqrt(C2(indx(j),indx(j))) )/(1-rho^2);
		contour(X,Y,chi2,[2.3,2.3],'k');
		lgd{1}=['Without SC'];
		
		hold off;
		
		if j==num_para xlabel(names{indx(i)}); end;
		if i==1 ylabel(names{indx(j)}); end;
	end;
end;

legend(lgd,'Position',[0.8,0.8,0.01,0.01]);
saveas(gca,'contour','png');